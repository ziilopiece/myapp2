package com.example.myapp2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputBinding
import com.example.myapp2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private var input1 = 0
    private var input2 = 0
    private var inputResult = 0
    private var operatorType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnTambah.setOnClickListener {
            changeOperator("+")
            operatorType = OperatorType.TAMBAH
        }
        binding.btnKurang.setOnClickListener {
            changeOperator("-")
            operatorType = OperatorType.KURANG
        }
        binding.btnKali.setOnClickListener {
            changeOperator("*")
            operatorType = OperatorType.KALI
        }
        binding.btnBagi.setOnClickListener {
            changeOperator("/")
            operatorType = OperatorType.BAGI
        }

        binding.btnResult.setOnClickListener {
            calculate()
        }
    }

    private fun changeOperator(operator: String) {
        binding.tvOperator.text = operator

    }

    private fun tambah() {
        inputResult = input1 + input2
    }

    private fun kurang() {
        inputResult = input1 - input2
    }

    private fun kali() {
        inputResult = input1 * input2
    }

    private fun bagi() {
        inputResult = input1 / input2
    }

    private fun calculate() {
        input1 = binding.etInput1.text.toString().toInt()
        input2 = binding.etInput2.text.toString().toInt()

        when (operatorType) {
            OperatorType.TAMBAH -> {
                tambah()
            }
            OperatorType.KURANG -> {
                kurang()
            }
            OperatorType.KALI -> {
                kali()
            }
            OperatorType.BAGI -> {
                bagi()
            }
        }
        binding.tvResul.text = inputResult.toString()
    }
}